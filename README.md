---
author: Benoît Launay
title: Perpignan
project: paged.js sprint
book format: letter
book orientation: portrait
---

# Theme name: Perpignan
## Description

The inspiration for this template comes from the early years of the printing industry, when printers traveled through Europe with their knowledge and presses! For the Northern Catalunya (now the French territory called *Roussilon*) I came accross the name of Jean Rozemberg[^JR], who was born in the city of Heidelberg (in Rhenish Palatinat, nowadays in Germany) and settled in Perpignan after some years in Gerona and Tarragonna (both cities are now part of the autonomous community of Catalunia in Spain).

## Typefaces

### Gentium Plus

more information on [software.sil.org](https://software.sil.org/gentium/design/)

## References

Perpignan:
    French mediterranean city near the Spanish border.

[^JR]: his forname must haven been Hans according to its german origin 
