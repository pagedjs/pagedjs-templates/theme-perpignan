class forensic extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        let panneau = document.createElement("aside");
        panneau.style = "position : fixed;top:30px;left:30px;border:1px solid purple;padding:5ch;z-index:999999;background-color:rgba(255,255,255,.8";
        panneau.insertAdjacentHTML(`beforeend`,
            `<p><strong>Classes dans la source</strong :</p>`
        );
        const sections = content.querySelectorAll('*');
        var classes = [];
        sections.forEach(element => {
            if (element.classList.value != '') {
                if (element.classList.value.includes(" ")) {
                    let coupage = element.classList.value.split(" ");
                    coupage.forEach(untag => {
                        classes.push(untag);
                    });

                } else {
                    classes.push(element.classList.value);
                }
            }
        });
        classes.sort();
        let uniqueClassStack = new Set(classes);
        uniqueClassStack.forEach(lesclaseses => {
            panneau.insertAdjacentHTML(`beforeend`,
                `<p>${lesclaseses}</p>`
            )
        });
        document.querySelector("body").appendChild(panneau);
    }
}
Paged.registerHandlers(forensic);